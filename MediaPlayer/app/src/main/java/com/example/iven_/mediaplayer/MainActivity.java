package com.example.iven_.mediaplayer;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity {

    MediaPlayer mediaplayer;
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        txt = (TextView) findViewById(R.id.textView);
        Log.i("Lifecycle","OnCreate called");
        mediaplayer = MediaPlayer.create(this,R.raw.song);
        mediaplayer.start();
        txt.setText("Playing");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Lifecycle","OnStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Lifecycle","OnResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Lifecycle","OnPause called");
        txt.setText("Pause");
        mediaplayer.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Lifecycle","OnStop called");
        txt.setText("Stop");
        mediaplayer.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Lifecycle","OnDestroy called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Lifecycle","OnRestart called");
    }
}
