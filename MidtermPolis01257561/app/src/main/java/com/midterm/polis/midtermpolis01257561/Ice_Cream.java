package com.midterm.polis.midtermpolis01257561;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Ice_Cream extends Activity {
    EditText name;
    EditText price;
    EditText qty;
    TextView result;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ice__cream);
        name = (EditText) findViewById(R.id.lblName);
        price = (EditText) findViewById(R.id.lblPrice);
        qty = (EditText) findViewById(R.id.lblQty);
        result = (TextView) findViewById(R.id.txtOutput);
        submit = (Button) findViewById(R.id.btnSubmit);

    }

    public void onSubmit(View view) {
        double num1=0;
        int   num2=0;

        try {

            num1 = Double.parseDouble(price.getText().toString());
            num2 = Integer.parseInt(qty.getText().toString());
            double sum = num1 * num2;
            String result2 = name.getText() + " ice cream (" + price.getText() +"$) X " + qty.getText() + " items = " + Double.toString(sum) + "$";
            result.setText(result2.toString());
            result.setVisibility(view.VISIBLE);
        } catch (NumberFormatException e) {
            result.setText("");
            Toast toast = Toast.makeText(this, "Please enter a vaild number for Price or Quantity", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }



    }

}
