package com.example.jdm.eggtimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SeekBar seekBarTimer;
    TextView textViewTimer;
    Button btnController;
    //to click go button to stop
    Boolean counterIsActive = false;
    CountDownTimer countDownTimer;

    public void resetTimer(){
        textViewTimer.setText("0:30");
        seekBarTimer.setProgress(30);
        countDownTimer.cancel();
        btnController.setText("Go!");
        seekBarTimer.setEnabled(true);
        //when go button is clicked, count down starts again
        counterIsActive = false;
    }
    public void updateTimer(int secondLeft){
        //i = progress
        int minutes = (int) secondLeft/60;
        int seconds = secondLeft-minutes * 60;

        String secondString = Integer.toString(seconds);

        textViewTimer.setText(Integer.toString(minutes) + ":"+ Integer.toString(seconds));

        if(seconds <= 9){

            secondString= "0" + secondString;
        }
        textViewTimer.setText(Integer.toString(minutes)+":"+secondString);
    }
    public void btnController(View view) {
        //when user wants to stop the count down
        if (counterIsActive == false) {

            //when button is clicked, which means "stop"
            counterIsActive = true;
            seekBarTimer.setEnabled(false);
            btnController.setText("Stop");

            countDownTimer = new CountDownTimer(seekBarTimer.getProgress() * 1000 + 100, 1000) {


                @Override
                public void onTick(long l) {

                    //l = millis Until Finished
                    updateTimer((int) l / 1000);
                }

                @Override
                public void onFinish() {
                    resetTimer();
                    MediaPlayer mplayer = MediaPlayer.create(getApplicationContext(), R.raw.airhorn);
                    mplayer.start();
                }
            }.start();

        }else{
           resetTimer();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView eggImageView =(ImageView) findViewById(R.id.eggImageView);
        textViewTimer = (TextView) findViewById(R.id.textViewTimer);
        btnController = (Button) findViewById(R.id.btnController);
        seekBarTimer = (SeekBar) findViewById(R.id.seekBarTimer);

        //set time: 600= 10m, 30= 30s
        seekBarTimer.setMax(600);
        seekBarTimer.setProgress(30);

        seekBarTimer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                updateTimer(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
