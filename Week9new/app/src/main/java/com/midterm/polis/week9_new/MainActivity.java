package com.midterm.polis.week9_new;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final int SELECTOR_CONST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onSelectColor(View view) {
        Intent myintent = new Intent(MainActivity.this, SelectColorActivity.class);
        startActivityForResult(myintent, SELECTOR_CONST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode){
            case SELECTOR_CONST:
                if(resultCode==RESULT_OK){
                    String color = data.getExtras().getString("selectedColor");
                    findViewById(R.id.colorBox).setBackgroundColor(Color.parseColor(color));
                }
        }
    }
}
