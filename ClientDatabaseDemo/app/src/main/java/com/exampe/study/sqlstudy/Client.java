package com.exampe.study.sqlstudy;

/**
 * Created by Volodymyr Voytenko on 27/03/2018.
 * Client.java - class to describe client
 */
public class Client {
    public int id;
    public String name;
    public String phone_number;

    public Client(int id, String name, String phone_number) {
        // TODO Auto-generated constructor stub
        this.id = id;
        this.name = name;
        this.phone_number = phone_number;
    }
    public Client(){
    }
}