package com.exampe.study.sqlstudy;
/**
 * Created by Volodymyr Voytenko on 27/03/2018.
 */
/**
 *   DatabaseHelper call used to create, upgrade database ,
 *   create table and perform C.R.U.D.  operations
 */
import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

    // Database Name
    public static String DATABASE_NAME = "client_database";
    // Current version of database
    private static final int DATABASE_VERSION = 1;
    // Name of table
    private static final String TABLE_CLIENT = "client";
    // All fields used in database table
    private static final String KEY_ID = "id";
    private static final String NAME = "name";
    private static final String PHONENUMBER = "phone_number";

    public static String TAG = "my_tag";

    // Client Table Create Query in this string
    private static final String CREATE_TABLE_CLIENT = "CREATE TABLE "
            + TABLE_CLIENT + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + NAME + " TEXT,"
            + PHONENUMBER + " TEXT );";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This method is called by system if the database is accessed but not yet
     * created.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CLIENT); // create client table
    }

    /**
     * This method is called when any modifications in database are done like
     * version is updated or database schema is changed
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_CLIENT); // drop table if exists
        onCreate(db);
    }

    /**
     * This method is used to add client detail in client Table
     * @param client
     * @return
     */

    public long addClientDetail(Client client) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(NAME, client.name);
        values.put(PHONENUMBER, client.phone_number);

        // insert row in client table

        long insert = db.insert(TABLE_CLIENT, null, values);
        return insert;
    }

    /**
     * This method is used to update particular client entry
     * @param client
     * @return
     */
    public int updateEntry(Client client) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(NAME, client.name);
        values.put(PHONENUMBER, client.phone_number);

        // update row in client table base on client.is value
        return db.update(TABLE_CLIENT, values, KEY_ID + " = ?",
                new String[] { String.valueOf(client.id) });
    }

    /**
     * Used to delete particular client entry
     * @param id
     */
    public void deleteEntry(long id) {
        // delete row in client table based on id
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CLIENT, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    /**
     * Used to get particular client details     *
     * @param id
     * @return
     */
    public Client getClient(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        // SELECT * FROM client WHERE id = ?;
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT + " WHERE "
                + KEY_ID + " = " + id;
        Log.d(TAG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Client client = new Client();
        client.id = c.getInt(c.getColumnIndex(KEY_ID));
        client.phone_number = c.getString(c.getColumnIndex(PHONENUMBER));
        client.name = c.getString(c.getColumnIndex(NAME));

        return client;
    }

    /**
     * Used to get detail of entire database and save in array list of data type
     * Clients
     *
     * @return
     */
    public List<Client> getAllClientsList() {
        List<Client> clientArrayList = new ArrayList<Client>();

        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT;
        Log.d(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                Client clients = new Client();
                clients.id = c.getInt(c.getColumnIndex(KEY_ID));
                clients.phone_number = c.getString(c
                        .getColumnIndex(PHONENUMBER));
                clients.name = c.getString(c.getColumnIndex(NAME));

                // adding to Clients list
                clientArrayList.add(clients);
            } while (c.moveToNext());
        }
        return clientArrayList;
    }
}