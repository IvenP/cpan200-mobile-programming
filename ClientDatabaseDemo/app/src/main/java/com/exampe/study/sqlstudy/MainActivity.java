package com.exampe.study.sqlstudy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    EditText name, phone_no, id;
    Button addButton, deleteButton;
    TextView tv;
    List<Client> list = new ArrayList<Client>();
    DataBaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DataBaseHelper(getApplicationContext());
        name = (EditText) findViewById(R.id.editText1);
        phone_no = (EditText) findViewById(R.id.editText2);
        id = (EditText) findViewById(R.id.editText3);
        addButton = (Button) findViewById(R.id.add);
        deleteButton = (Button) findViewById(R.id.delete);
        tv = (TextView) findViewById(R.id.tv);
        addButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);

        //add some clients at the very beginning
        Client client = new Client();
        client.name = "Anna";
        client.phone_number = "416-5556789";
        db.addClientDetail(client);
        client.name = "Eric";
        client.phone_number = "905-3435677";
        db.addClientDetail(client);



        list = db.getAllClientsList();
        printAll(list);

    }

    private void printAll(List<Client> list) {
        //  print all clients intro the screen
        String value = "";
        for (Client sm : list) {
            value = value + "Id: " + sm.id + ", Name: " + sm.name + " , Phone: " + sm.phone_number + "\n";
        }
        tv.setText(value);
    }

    @Override
    public void onClick(View v) {
        // button delete pressed
        if (v == findViewById(R.id.delete)) {
            tv.setText("");
            String client_id = id.getText().toString();
            db.deleteEntry(Integer.parseInt(client_id));
            list = db.getAllClientsList();
            printAll(list);
        }
        // button add pressed
        if (v == findViewById(R.id.add)) {
            tv.setText("");
            Client client = new Client();
            client.name = name.getText().toString();
            client.phone_number = phone_no.getText().toString();
            db.addClientDetail(client);
            list = db.getAllClientsList();
            printAll(list);
        }
    }
}