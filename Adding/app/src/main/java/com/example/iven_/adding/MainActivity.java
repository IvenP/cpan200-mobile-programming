package com.example.iven_.adding;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText firstNum;
    EditText secondNum;
    TextView result;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstNum = (EditText) findViewById(R.id.edit1);
        secondNum = (EditText) findViewById(R.id.edit2);
        result = (TextView) findViewById(R.id.txtResult);
        btnAdd = (Button) findViewById(R.id.btnAdd);
    }

    public void onAdd(View view) {
        int num1, num2;
        num1 = Integer.parseInt(firstNum.getText().toString());
        num2 = Integer.parseInt(secondNum.getText().toString());
        int sum = num1 + num2;
        result.setText(Integer.toString(sum));

    }
}
