package com.example.iven_.mytap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    int clicks = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void onlick(View view) {
    TextView text = findViewById(R.id.textView);
    clicks++;
    text.setText("Button Clicked!" + clicks + " times");

        Toast.makeText(MainActivity.this,
                "Button Clicked!", Toast.LENGTH_LONG).show();
    }
}
