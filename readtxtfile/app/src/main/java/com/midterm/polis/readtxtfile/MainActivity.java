package com.midterm.polis.readtxtfile;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.InputStream;
import java.util.Scanner;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSearch(View view) {
        EditText word = (EditText) findViewById(R.id.editText);
        String theword = word.getText().toString();

        String definition = findDefenition(theword);
        TextView thedef = (TextView) findViewById(R.id.textView);

        if(definition!=null)
            thedef.setText(definition);
        else
            thedef.setText("Word not Found");
    }

    private String findDefenition(String theword) {
        InputStream input = getResources().openRawResource(R.raw.word);
        Scanner scan = new Scanner(input);

        while (scan.hasNext()){
            String line = scan.nextLine();
            String[] pieces = line.split("=");

            if(pieces[0].equalsIgnoreCase(theword.trim())){
                return pieces[1];
            }


        }
    return null;

    }
}
