package com.midterm.polis.week9_senddataback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

public class SelectColorActivity extends Activity {
    private String color = "#ff0000";
    Intent selectedColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_color);
        setTitle("Select Color Activity");

        RadioGroup colors = (RadioGroup) findViewById(R.id.radioGroup);
        Button ok = (Button) findViewById(R.id.button);
        selectedColor = new Intent();

        colors.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioButton5:
                        color = "#ff0000"; break;
                    case R.id.radioButton4:
                        color = "#0000ff"; break;
                    case R.id.radioButton3:
                        color = "#00ff00"; break;
                    case R.id.radioButton2:
                        color = "#f0cf00"; break;
                    case R.id.radioButton:
                        color = "#c0c0ff"; break;

                }
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedColor.putExtra("selectedColor", color);
                setResult(RESULT_OK,selectedColor);
                finish();
            }
        });
    }
}
