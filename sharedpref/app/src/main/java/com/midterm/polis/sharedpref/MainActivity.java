package com.midterm.polis.sharedpref;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
    EditText name,age;
    SharedPreferences myprefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myprefs = getPreferences(MODE_PRIVATE);
        init();
    }
    private void init(){
        name = (EditText) findViewById(R.id.editName);
        age = (EditText) findViewById(R.id.editAge);
        readPreferences();
    }

    public void onSave(View view) {
        String nameText = name.getText().toString();
        int ageText = Integer.parseInt((age.getText().toString()));

        SharedPreferences.Editor editor = myprefs.edit();
        editor.putString(("keyname"),nameText);
        editor.putInt(("keyage"),ageText);
        editor.commit();
    }

    public void onReset(View view) {
        SharedPreferences.Editor editor = myprefs.edit();
        editor.clear();
        editor.commit();
        readPreferences();
    }
    public void readPreferences(){
        String st1 = myprefs.getString("keyname","");
        name.setText(st1);
        int val1 = myprefs.getInt("keyage",0);
        age.setText(String.valueOf(val1));
    }
}
